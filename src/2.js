/*
Напишите функцию deepEqual(val1, val2) которая производит “глубокое” сравнение двух значений,
и возвращает результат сравнения (true или false). val1 и val2 могут быть любого типа (булевы
значения, числа, строки, массивы, объекты, функции) эквивалентность функций означает что код
этих функций - одинаковый.
*/

const eq = (l, r) => l === r

/** Checks arrays metadata
 * @param  {any[]} l
 * @param  {any[]} r
 * @returns {boolean} true if both are arrays with the same length
 */
function areBothArraysSameLength (l, r) {
  if (Array.isArray(l)) {
    if (!Array.isArray(r)) {
      return false
    } else {
      if (l.length != r.length) return false
    }
  }
  return true
}

/** Deeply compares objects to be equal
 * @param  {object} l
 * @param  {object} r
 * @returns {boolean} true - arguments are deep equal, false - otherwise
 */
function areObjectsEqual (l, r) {
  if (l === null) {
    return r === null
  } else {
    if (r === null) return false
  }
  if (typeof r !== 'object') return false
  if (areBothArraysSameLength(l, r) == false) return false

  const names = Object.keys(l)
  if (names.length == 0 && Object.keys(r).length != 0) return false

  for (const name of names) {
    const rightValue = r[name]
    // const isEqual = rightValue !== undefined && deepEqual(l[name], rightValue)
    let isEqual = false
    if (rightValue === undefined) {
      if (l[name] !== undefined) return false
      isEqual = true
    } else if (deepEqual(l[name], rightValue)) {
      isEqual = true
    }

    if (!isEqual) return false
  }
  return true
}

/** Compares functions to be equal
 * @param  {function} l - left argument
 * @param  {function} r - right argument
 * @returns {boolean} true - functions are the same or have same code, false - functions differ
 */
function areFunctionsEqual (l, r) {
  if (l !== r) {
    // compare code
    if (typeof r === 'function') return eq(l.toString(), r.toString())
    else return false
  }
  return true
}

/** Deeply compares arguments to be equal (===)
 * @param  {object} l
 * @param  {object} r
 * @returns {boolean} true - arguments are deep equal, false - otherwise
 */
function deepEqual (l, r) {
  const handlers = {
    boolean: eq,
    number: eq,
    string: eq,
    function: areFunctionsEqual,
    undefined: eq,

    object: areObjectsEqual
  }
  return handlers[typeof l](l, r)
}

module.exports = {
  default: deepEqual,
  deepEqual,
  impl: {
    areBothArraysSameLength,
    areFunctionsEqual,
    areObjectsEqual,
    eq
  }
}
