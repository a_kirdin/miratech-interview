/*
Написать код, после выполнения которого, возможны след. операции:
[1, 2, 3].toList() которая превращает массив любых значений в структуру след вида
var list = {
	head: 1,
	tail: {
		head: 2,
		tail: {
			head: 3,
			tail: undefined
		}
	}
}
list.toArray() которая делает обратную операцию => [1, 2, 3]
list.nth(n_elem)  возвращает значение n-го элемента от начала списка (нумерация начиная с 0).

*/

function makeNode (data) {
  return {
    head: data,
    tail: undefined,
    toArray: function _toArray () {
      const arr = [this.head]

      let item = this.tail
      for (; item; item = item.tail) {
        arr.push(item.head)
      }
      return arr
    },
    nth: function _nth (nthElem = 0) {
      if (nthElem == 0) return this.head

      let item = this.tail
      for (let it = 1; it < nthElem && item !== undefined; it++) {
        item = item.tail
      }
      return item != undefined ? item.head : item
    }
  }
}

/** Converts array pointed by 'this' to a list object
 * @returns {{head: any, tail: undefined|{head: any, tail: any}}|undefined} - returns list from array, or undefined for empty array
 */
function toListMixin () {
  if (Array.isArray(this)) {
    const arr = this

    let newList
    let prevNode = null
    for (const rec of arr) {
      const node = makeNode(rec)
      if (prevNode !== null) {
        prevNode.tail = prevNode = node
      } else {
        newList = node
      }
      prevNode = node
    }
    return newList
  }
  return undefined
}

function addArrayMixin () {
  if (undefined === Array.prototype['toList']) {
    Array.prototype['toList'] = toListMixin
  }
}

module.exports = {
  default: addArrayMixin,

  addArrayMixin,
  makeNode
}
