/*
5. Написать функцию logIt(obj) на вход которой поступает объект вида (пример)
var complexNum = { r: 1, i: 2, mod: function() { return Math.sqrt(this.r * this.r + this.i * this.i); }, add: function(r, i) { this.r += r; this.i += i; } };

Функция модифицирует его таким образом, что потом, при вызове каждого метода, происходит
распечатка в консоли браузера имени вызванного метода и его аргументов.
 */

const { bindAll } = require('../src/1')

var complexNum = {
  r: 1,
  i: 2,
  mod: function () {
    return Math.sqrt(this.r * this.r + this.i * this.i)
  },
  add: function (r, i) {
    this.r += r
    this.i += i
  }
}

function logItMixin (obj, fnName, ...args) {
  console.log(`logIt: called '${fnName}', params: '${args}'`)
}

function logIt (arg) {
  return bindAll(arg, [logItMixin])
}

module.exports = {
  complexNum,
  logIt
}
