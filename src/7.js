/*
Написать функцию curry которая на вход принимает функцию f с n аргументами (n >= 1) и возвращает
 функцию g с 1 аргументом. Причем, первоначальный вызов f с n аргументами можно выполнить
 путем n вызовов g c 1 аргументом. например:

function add3(a, b, c) { return a + b + c; }
var g = curry(add3);
g(1); g(2); g(3) => третий вызов вернет 6
добавить возможность “сцепления”
g(1)(2)(3); => 6
*/

/** Wraps 'fn' with a function with 1 argument.
 * @param  {function} fn - fucntion to transform
 */
function curry (fn) {
  const fnStored = fn
  const state = {
    accumulate: undefined,
    args: [],
    freeIndex: 0
  }

  function doStuff (param) {
    state.args.push(param)
    if (state.args.length >= fnStored.length) {
      const result = fnStored(...state.args)

      return result
    }
    return doStuff
  }

  return doStuff
}

module.exports = {
  curry
}
