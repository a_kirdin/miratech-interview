/*
Написать функцию seq которая на вход получает пользовательскую функцию f, а на выходе возвращает
функцию, вызывая ее, каждый раз можно получить последовательность чисел, определяемую пользовательской
 функцией f. Функция f принимает на вход в качестве аргументов результаты всех своих предыдущих вызовов.
 Порядок аргументов такой: слева находится результат предыдущего вызова, потом результат
 пред-предыдущего и т.д. Например, для генерации чисел Фибоначчи можно использовать след код:

var fib = seq(function(n1, n2) { return (n1 + n2) || 1; });
fib() => 1
fib() => 1
fib() => 2
fib() => 3
fib() => 5 и т.д.
 */

function seq (fn) {
  var gen = fn
  var state = {
    results: []
  }
  function _seq () {
    var nextValue = gen(...state.results)
    state.results.unshift(nextValue)

    return nextValue
  }

  return _seq
}

module.exports = {
  default: seq,
  seq
}
