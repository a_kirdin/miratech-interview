/**
 Предположим есть составной объект:
var obj = {
	x: 6,
	inner: {
		a: ‘test’,
		b: [1, 2, 3]
	}
}
Написать функцию extract(obj, strPath) которая извлекает данные из такого объекта по пути:
extract(obj, ‘x’) => 6
extract(obj, ‘inner.a’) => ‘test’
extract(obj, ‘inner.b[2]’) => 3
extract(obj, ‘inner[‘b’][2]’) => 3 (доступ к свойству объекта через [ ])
PS: eval использовать нельзя! (решение в этом случае - 1 строка)

 */

/** Builds path from a string
 * @param  {string} str
 */
function buildPath (str = '') {
  const startIndex = '['
  const endIndex = ']'
  const stringLimits = `'"`
  const dot = '.'
  const delimiters = [startIndex, endIndex, ...stringLimits, dot]
  var sourceString = str

  const foundPath = []

  const types = ['none', 'prop', 'index', 'string']

  function newState (type = 'none') {
    if (!types.includes(type)) {
      throw new Error(`Invalid type parameter passed: ${type}`)
    }

    var state = {
      type,
      startPos: -1,
      endPos: -1,
      literal: [],
      addChar: function (p, it) {
        if (this.literal.length == 0) this.startPos = it
        this.literal.push(p)
        this.endPos = it

        const isNoneAndJustStarted = this.type == 'none' && this.startPos == it

        if (isNoneAndJustStarted) {
          const notDelimiter = delimiters.find(rec => rec == p) == undefined

          if (notDelimiter) {
            this.type = 'prop'
          }
        }
        return this
      },
      makePath: function () {
        if (this.type == 'index') {
          // remove []
          let slicer = [1, -1]
          // if surrounded by "", remove either
          if (this.literal.length >= 4) {
            const arr = [this.literal[1], this.literal[this.literal.length - 2]]
            if (arr.every(char => [...stringLimits].includes(char))) {
              slicer = [2, -2]
            }
          }
          return this.literal.slice(...slicer).join('')
        }
        return this.literal.join('')
      },
      tail: null
    }
    return state
  }
  const storage = []

  const handlers = {
    startIndex: function startIndexHandler (state, p, it) {
      // TODO: [ inside of strings
      if (state.type === 'prop') {
        const ns = newState('index')
        ns.addChar(p, it)
        storage.push(state)

        return ns
      } else {
        // /fills this.literal with ["0"], [0]
        state.type = 'index'
        return state.addChar(p, it)
      }
    },
    endIndex: function endIndexHandler (state, p, it) {
      // TODO: ] inside of strings
      state.addChar(p, it)
      storage.push(state)
      return newState()
    },
    dot: function startProp (state, p, it) {
      if (state.type === 'string') {
        return this.addChar(p, it)
      } else {
        if (state.type == 'none') {
          // reuse
          state.startPos = it
          state.endPos = it
        } else {
          // create new
          const ns = newState('prop')
          ns.startPos = it
          ns.endPos = it

          storage.push(state)
          return ns
        }
        return state
      }
    },
    stringLimit: function stringLimitHandler (state, p, it) {
      return state.addChar(p, it)
    },
    undefined: function addLiteral (state, p, it) {
      return state.addChar(p, it)
    }
  }

  let state = newState()
  const firstChar = str.charAt(0)

  if (firstChar === startIndex) {
    state.type = 'index'
  }
  for (let it = 0; it < str.length; it++) {
    const p = str.charAt(it)
    switch (p) {
      case startIndex:
        state = handlers['startIndex'](state, p, it)
        break
      case endIndex:
        state = handlers['endIndex'](state, p, it)
        break
      case stringLimits[0]:
        state = handlers['stringLimit'](state, p, it)
        break
      case stringLimits[1]:
        state = handlers['stringLimit'](state, p, it)
        break
      case dot:
        state = handlers['dot'](state, p, it)
        break
      default:
        state = handlers['undefined'](state, p, it)
    }
  }

  if (state.startPos > -1) storage.push(state)
  return storage
}

function extract (obj, strPath) {
  const pathFull = buildPath(strPath)
  const path = pathFull.map(state => state.makePath())

  const value = {}
  const acc = path.reduce(
    (acc, val) => {
      if (acc != undefined) {
        acc.obj = acc.obj[val]
        acc.value = val
      }
      return acc
    },
    { obj, value }
  )

  return acc ? acc.obj : null
}

function extractEval (obj, strPath) {
  try {
    let toEval = ''
    if (strPath.length > 0 && strPath.charAt(0) != '[') {
      toEval = `obj.${strPath}`
    } else {
      toEval = `obj${strPath}`
    }

    const res = eval(toEval)
    return res
  } catch (err) {
    console.error(`Error: ${err.stack}`)
    throw err
  }
}

module.exports = {
  extract,
  extractEval,
  buildPath
}
