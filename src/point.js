/** Miratech interview part *
 */

class Point {
  constructor (x = 0, y = 0) {
    this.x = x
    this.y = y
  }

  mod () {
    return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2))
  }
}

function mybind (p, fnName) {
  return function () {
    return p[fnName]
  }
}

function pointTest () {
  var p = new Point(10, 0)
  var f = mybind(p, 'mod')

  var res = [f, f(), p.mod, p.mod()]
  console.log(res)
  console.log(f)
  console.log(f())

  return new Point(10, 0).mod() == 10
}

module.exports = {
  test: pointTest,
  Point,
  mybind
}
