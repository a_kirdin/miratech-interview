/*
Написать функцию bindAll(obj) которая получает на вход объект вида
var complexNum = { r: 1, i: 2, mod: function() { return Math.sqrt(this.r * this.r + this.i * this.i); } };
И переделывает все его методы таким образом что код:
var mod= complexNum.mod;
mod() === complexNum.mod() вернет истину (то есть mod будет работать одинаково в обоих случаях)
 */

function bindOne (obj, fnName, mixins) {
  var originalFn = obj[fnName]
  var passedMixins = mixins || []

  return function (...args) {
    for (const mixin of passedMixins) {
      mixin(obj, fnName, ...args)
    }
    return originalFn.bind(obj)(...args)
  }
}

function bindAll (args, mixins) {
  if (typeof args === 'object') {
    const names = Object.keys(args).filter(
      name => typeof args[name] === 'function'
    )

    names.forEach(name => (args[name] = bindOne(args, name, mixins)))
    return args
  }
  return args
}

module.exports = {
  bindAll,
  bindOne
}
