const { expect } = require('chai')
const { curry } = require('../src/7')
const debug = require('debug')('test:7-test')

function add3 (a, b, c) {
  return a + b + c
}

const testCasesAdd3 = [
  {
    data: [1, 2, 3],
    result: 6,
    info: 'Three elements'
  },
  {
    data: [100, 1, -100],
    result: 1,
    info: 'Three elements'
  },
  {
    data: [100, 1, -100, -100],
    result: 1,
    info: 'Four elements, last element ignored'
  }
]

describe(`# Case 7: curry`, () => {
  describe(`# iterative calls`, () => {
    testCasesAdd3.forEach(rec => {
      const example = add3(...rec.data)

      it(`Run add3 test: arr: ${rec.data}, result: ${rec.result}, info: '${
        rec.info
      }'`, () => {
        const gen = curry(add3)

        const curriedResults = rec.data.map(n => gen(n))
        const result = curriedResults[curriedResults.length - 1]
        expect(example).to.equal(result)
      })
    })
  })

  // chaining
  describe(`# chaining`, () => {
    testCasesAdd3.forEach(rec => {
      const example = add3(...rec.data)

      it(`Run add3 test: arr: ${rec.data}, result: ${rec.result}, info: '${
        rec.info
      }'`, () => {
        const gen = curry(add3)

        const result = gen(rec.data[0])(rec.data[1])(rec.data[2])
        expect(example).to.equal(result)
      })
    })
  })
})
