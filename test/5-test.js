const { expect } = require('chai')
const { logIt, complexNum } = require('../src/5')
const debug = require('debug')('test:5-test')

describe(`# Case 5: logIt modifies object methods to log its name and arguments on call`, () => {
  it(`call mixed-in code on every method`, () => {
    const complex = { ...complexNum }

    const original = complexNum.mod()

    logIt(complex)

    const bound = complex.mod()

    const originalAdd = complexNum.add(4, 4)
    const boundAdd = complex.add(4, 4)

    debug(`Bound 'mod': ${bound}, original: ${original}`)
    debug(`Bound 'add': ${boundAdd}, original: ${originalAdd}`)
    expect(bound).to.be.equal(original)
  })
})
