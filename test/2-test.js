const { expect } = require('chai')
const { deepEqual } = require('../src/2.js')
const debug = require('debug')('test:2-test')

const identity = v => v
const identity2 = k => k

describe(`# case 2: deepEqual`, () => {
  const fn = () => ({})
  const fnCopy = () => ({})

  const fn2 = () => 2

  const emptyObj = {}
  const emptyArr = []
  const primitives = [
    { type: 'boolean', left: true, right: true, result: true, info: 'same' },
    {
      type: 'boolean',
      left: true,
      right: false,
      result: false,
      info: 'different'
    },
    { type: 'number', left: 1, right: 1.0, result: true, info: 'same' },
    { type: 'number', left: 1, right: 2.0, result: false, info: 'different' },
    { type: 'string', left: '1', right: '1', result: true, info: 'same' },
    {
      type: 'string',
      left: '1',
      right: '2.0',
      result: false,
      info: 'different'
    },

    { type: 'function', left: fn, right: fn, result: true, info: 'same' },
    {
      type: 'function',
      left: fn,
      right: fnCopy,
      result: true,
      info: 'different functions, same function code'
    },
    {
      type: 'function',
      left: fn,
      right: fn2,
      result: false,
      info: 'different functions'
    },
    {
      type: 'function',
      left: fn,
      right: undefined,
      result: false,
      info: 'functions != undefined'
    },
    {
      type: 'function',
      left: fn,
      right: null,
      result: false,
      info: 'functions != null'
    },
    {
      type: 'function',
      left: fn,
      right: 1,
      result: false,
      info: 'functions != 1'
    },

    {
      type: 'object',
      left: emptyObj,
      right: emptyObj,
      result: true,
      info: 'same object'
    },
    {
      type: 'object',
      left: emptyObj,
      right: { a: 1 },
      result: false,
      info: 'different objects, different fields'
    },

    {
      type: 'object',
      left: emptyArr,
      right: [],
      result: true,
      info: 'empty arrays'
    },
    {
      type: 'object',
      left: emptyArr,
      right: [...emptyArr, 1],
      result: false,
      info: 'different length arrays'
    },
    {
      type: 'null',
      left: null,
      right: identity(null),
      result: true,
      info: 'null values'
    }
  ]

  describe(`# test primitives`, () => {
    primitives.forEach(rec => {
      it(`test type='${rec.type}', info='${rec.info}'`, () => {
        const val = deepEqual(rec.left, rec.right)
        debug(rec.left, rec.right, val)
        expect(val).to.be.equal(rec.result)
      })
    })
  })

  const arr = [{ a: 1, b: 1 }]

  const nested = [
    {
      type: 'object',
      left: arr[0],
      right: arr[0],
      result: true,
      info: 'same object'
    },
    {
      type: 'object',
      left: arr[0],
      right: { a: 1, b: 1 },
      result: true,
      info: 'same fields'
    },
    {
      type: 'object',
      left: arr[0],
      right: { a: 1, b: 2 },
      result: false,
      info: 'diff fields'
    },
    {
      type: 'object',
      left: arr[0],
      right: { a: 1, b: undefined },
      result: false,
      info: 'right has another undefined field'
    },
    {
      type: 'object',
      left: { ...arr[0], c: undefined },
      right: { a: 1, b: 1, c: undefined },
      result: true,
      info: 'both have same undefined fields'
    },

    {
      type: 'array',
      left: [],
      right: [],
      result: true,
      info: 'empty arrays'
    },
    {
      type: 'array',
      left: [1, 2, 3],
      right: [identity(1), identity(2), identity(3)],
      result: true,
      info: 'empty arrays, right has calculated values'
    },
    {
      type: 'array',
      left: [1, 2, 3],
      right: [identity(1).toString(), identity(2), identity(3)],
      result: false,
      info: 'different arrays'
    },
    {
      type: 'array',
      left: [identity],
      right: [identity],
      result: true,
      info: 'array of different functions, which differs only with variable names'
    }
  ]

  describe(`# test nested objects`, () => {
    nested.forEach((rec, idx) => {
      it(`test type='${rec.type}, info: '${rec.info}', idx: ${idx}'`, () => {
        const val = deepEqual(rec.left, rec.right)
        debug(rec.left, rec.right, val)
        expect(val).to.be.equal(rec.result)
      })
    })
  })
})
