const { expect } = require('chai')
const { seq } = require('../src/4')
const debug = require('debug')('test:4-test')

describe(`# Case 4: seq`, () => {
  describe(`simple add`, () => {
    function add (n0, n1, ...args) {
      return n1 + n0 || 1
    }

    const seqArray = []

    const fibArray = []

    const gen = seq(add)

    for (let i = 0; i < 20; i++) {
      const next = gen()
      seqArray.unshift(next)

      const otherNext = add(...fibArray.slice(0, 2))

      fibArray.unshift(otherNext)
      console.log(`generated: ${next}, straight 'add' call: ${otherNext}`)
    }
    it(`compare generated arrays`, () => {
      expect(fibArray).to.be.deep.equal(seqArray)
    })
  })
})
