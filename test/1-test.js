const { expect } = require('chai')
const { bindAll } = require('../src/1')
const debug = require('debug')('test:1-test')

function makeComplex () {
  var complexNum = {
    r: 1,
    i: 2,
    mod: function () {
      return Math.sqrt(this.r * this.r + this.i * this.i)
    },
    distance: function (r, i) {
      return Math.sqrt(this.r * this.r - r * r + (this.i * this.i - i * i))
    }
  }

  return { ...complexNum }
}

describe(`#Test case 1: bindAll`, () => {
  it(`bindAll`, () => {
    // bindAll code
    expect(bindAll).to.be.a('function')
  })
  it(`bindAll: replace methods`, () => {
    const complexNum = makeComplex()
    debug(`Before bindAll: `, complexNum.mod())
    bindAll(complexNum)

    const mod = complexNum.mod
    debug(`After bindAll: `, complexNum.mod())
    debug(mod())
    const result = mod()
    expect(result).to.be.equal(complexNum.mod())
    expect(result).to.be.a('number')
  })
  it(`bindAll: replace methods two times`, () => {
    const complexNum = makeComplex()
    debug(`Before bindAll: `, complexNum.mod())
    bindAll(bindAll(complexNum))

    const mod = complexNum.mod
    debug(`After bindAll: `, complexNum.mod())
    debug(mod())
    const result = mod()
    expect(result).to.be.equal(complexNum.mod())
    expect(result).to.be.a('number')
  })

  it(`bindAll: method with 2 parameters, distance`, () => {
    const complexNum = makeComplex()
    bindAll(complexNum)

    const distance = complexNum.distance(0, 0)
    debug(`distance: `, distance)
    expect(distance)
      .to.be.a('number')
      .equal(complexNum.mod())
  })
})
