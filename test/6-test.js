const { expect } = require('chai')
const { extractEval, extract, buildPath } = require('../src/6')
const debug = require('debug')('test:6-test')

function testObject () {
  var obj = {
    x: 6,
    inner: {
      a: 'test',
      b: [1, 2, 3]
    }
  }
  return obj
}

const testPaths = [
  { path: 'x', result: 6, props: ['x'] },
  { path: 'inner.a', result: 'test', props: ['inner', 'a'] },
  { path: 'inner.b[2]', result: 3, props: ['inner', 'b', '2'] },
  { path: 'inner["b"][2]', result: 3, props: ['inner', 'b', '2'] },
  { path: '["x"]', result: 6, props: ['x'] },
  { path: '["inner"].a', result: 'test', props: ['inner', 'a'] },
  { path: '["inner"].b[2]', result: 3, props: ['inner', 'b', '2'] },
  { path: '["inner"]["b"][2]', result: 3, props: ['inner', 'b', '2'] }
]
const testPaths1 = [
  { path: '["inner"].a', result: 'test', props: ['inner', 'a'] },
  { path: '["inner"].b[2]', result: 3, props: ['inner', 'b', '2'] }
]

describe(`Case 6: extract() field by string path`, () => {
  describe(`buildPath() for test`, () => {
    const obj = testObject()
    testPaths.forEach(rec => {
      it(`buildPath(obj, ${rec.path})`, () => {
        const storage = buildPath(rec.path)
        debug(`Path built from '${rec.path}: ', result: `, storage)

        const pathMade = storage.map(state => state.makePath())
        debug(`Path built from '${rec.path}: ', result: `, pathMade)

        expect(pathMade).to.be.deep.equal(rec.props)
      })
    })
  })
  describe.skip(`extractEval() for test`, () => {
    const obj = testObject()
    testPaths.forEach(rec => {
      it(`extractEval(obj, ${rec.path} -> ${rec.result})`, () => {
        const result = extractEval(obj, rec.path)

        expect(result).to.be.equal(rec.result)
      })
    })
  })
  describe(`extractEval() for test`, () => {
    const obj = testObject()
    testPaths.forEach(rec => {
      it(`extract(obj, ${rec.path} -> ${rec.result})`, () => {
        const result = extract(obj, rec.path)

        expect(result).to.be.equal(rec.result)
      })
    })
  })
})
