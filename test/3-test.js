const { expect } = require('chai')
const beforeAddMixin = Array.prototype['toList']
const { addArrayMixin, makeNode } = require('../src/3')
const debug = require('debug')('test:3-test')

addArrayMixin()

const afterAddMixin = Array.prototype['toList']

const arr = [10, 20, 30, 'aaa']
const list = arr.toList()
console.log('toList on array: ', arr)
console.dir(list, { depth: 10 })

describe(`# Case 3: Array.toList mixin`, () => {
  it(`before mixin toList is undefined`, () => {
    expect(beforeAddMixin).to.be.undefined
  })

  it(`Array.toList mixed in to Array.prototype`, () => {
    expect(Array.prototype['toList']).to.be.a('function')
  })
  it(`make list of array`, () => {
    const list = arr.toList()
    const testNode = makeNode(1)
    const nodeKeys = Object.keys(testNode)
    debug(list)
    debug(testNode)
    debug(nodeKeys)

    expect(list).have.all.keys(nodeKeys)

    let item = list
    while (item.tail != undefined) {
      expect(item).have.all.keys(nodeKeys)
      item = item.tail
    }
  })
  describe(`toArray `, () => {
    it(`make array back from list`, () => {
      const list = arr.toList()
      debug(list)

      expect(list).to.be.an('object')

      const arrFromList = list.toArray()
      debug(arrFromList)
      expect(arrFromList).to.be.an('array')

      expect(arrFromList).to.be.deep.equal(arr)
    })
    it(`list from empty array`, () => {
      const arr = []
      const list = arr.toList()

      debug(list)
      expect(list).to.be.undefined
    })
  })

  describe(`nth element`, () => {
    it(`get 3-rd element`, () => {
      const list = arr.toList()

      const index = 1
      const nth = list.nth(index)
      debug(`index: ${index}, arr[${index}] == '${arr[index]}', nth: '${nth}' `)
      expect(nth).to.be.equal(arr[index])
    })
  })

  // release mixin
  after(() => {
    Array.prototype['toList'] = undefined
  })
})
